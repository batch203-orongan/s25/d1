// console.log("Hello, World!");

// [SECTION] JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- Common use of JSON is to read data from a web server, and display the data in the webpage.
	 - Features of JSON
	 	- It is a lighweight data-interchange format.
	 	- It is easy to read and write. 
	 	- It is easy for machines to parse and generate.
*/

// JSON Objects
//  - JSON is also used the "key/value pairs" just like the object properties in JacaScript.
//  - "Key/Properties" names requires to be enclosed with double qoutes.
/*
	Syntax:
	{
		"propertyA" : "ValueA",
		"propertyB" : "ValueB"
	}
*/
// Example of a JSON Object
// {
// 	" city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"couontry" : "Philippines"
// }

// [SECTION] JSON Array
// Arrays in JSON are almost same as arrays in javascript
// Arrays of JSON object

// "cities" : [
// 	{
// 	" city" : "Quezon City",
// 	"province" : "Metro Manila",
// 	"couontry" : "Philippines"
// 	},
// 	{
// 	" city" : "Manila City",
// 	"province" : "Metro Manila",
// 	"couontry" : "Philippines"
// 	},
// 	{
// 	" city" : "Makati City",
// 	"province" : "Metro Manila",
// 	"couontry" : "Philippines"
// 	}		
// ]

// [SECTION] JSON Methods
// The "JSON Object" contains methods for parsing and converting data into stringified JSON
//  JSON data is sent or recieved in text-only(String) format

// Converting data Into Stringified JSON
	
	// JavaScript Array of objects
	let batchesArr = [
		{
			batchName: "Batch 203",
			schedule: "Full Time"
		},
		{
			batchName: "Batch 204",
			schedule: "Part Time"
		}
	]

	console.log(batchesArr);

	// The "Stringify" method is used to convert javascript object into a string.
	console.log("Result from the stringify method: ");
	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "John",
		age : 31,
		address: {
			city: "Manila",
			country: "Philippines"
		}
	})

	console.log(data);

	// User details
	// let firstName = prompt("Enter your first name:");
	// let lastName = prompt("Enter your last name:");
	// let email = prompt("Enter your email:");
	// let password = prompt("Enter your password:");

	// let otherData = JSON.stringify({
	// 	firstName: firstName,
	// 	lastName: lastName,
	// 	email: email,
	// 	password: password
	// })

	// console.log(otherData);

//  [SECTION] Converting Stringified JSON into a Javascript Objects

let batchesJSON = `[
		{
			"batchName": "Batch 203",
			"schedule": "Full Time"
		},
		{
			"batchName": "Batch 204",
			"schedule": "Part Time"
		}
	]`

	console.log("batchesJSON content:");
	console.log(batchesJSON);

	// JSON.parse method to convert JSON Objects into a Javascript Objects
	console.log("Result from parse method:");
	// console.log(JSON.parse(batchesJSON));
	let parseBatches = JSON.parse(batchesJSON);
	console.log(parseBatches[0].batchName);

	let stringifiedObject = `{
		"name":"John",
		"age":31,
		"address":{
			"city":"Manila",
			"country":"Philippines"}
		}`

	console.log(stringifiedObject);
	console.log(JSON.parse(stringifiedObject));